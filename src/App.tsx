import React, { PureComponent } from 'react';

import UILayout from './components/UI/UILayout/UILayout';

import styles from './App.module.scss';

class App extends PureComponent {
  render () {
    return (
      <div className={styles.view}>
        <header className={styles.header}>
          <div className={styles.name}>Clément Lucas</div>
        </header>
        <UILayout vertical gap={2} align space="center">
          <div>Coucou</div>
        </UILayout>
      </div>
    );
  }
}

export default App;
