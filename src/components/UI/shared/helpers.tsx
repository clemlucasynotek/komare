export const cn = (...args: Array<string | undefined | null>) =>
  args
    .filter(Boolean)
    .join(' ')
    .trim();
