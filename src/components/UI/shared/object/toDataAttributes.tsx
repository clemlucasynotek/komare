import { toPairs as entries, into, map, adjust, compose, filter } from 'ramda';
import kebabCase from '../string/kebabCase';

type KeyType = string | number | boolean | null | undefined;
const mapKeys = (fn: any, obj: R.Dictionary<KeyType>) => into({}, map(adjust(0, fn)), entries(obj));
const prependData = (str: string) => `data-${str}`;

const toDataAttributes = (obj: R.Dictionary<KeyType>) =>
  mapKeys(compose(prependData, kebabCase), filter(Boolean, obj));

export default toDataAttributes;
