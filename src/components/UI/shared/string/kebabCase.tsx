const kebabCase = (s: string) => s
.replace(/([a-z])([A-Z])/g, '$1-$2')
.replace(/\s+/g, '-')
.toLowerCase();

export default kebabCase;
