Color was added in the example below to help vizualize how props affect the component.

Exemple without any props, `horizontal` is default

```jsx
const { default: Dummy } = require('src/components/UI/Styleguide/Dummy/');
const style = { backgroundColor: 'lightpink' };
<UILayout style={style}>
  <Dummy>Block 1</Dummy>
  <Dummy>Block 2</Dummy>
</UILayout>;
```

Exemple with vertical prop:

```jsx
const { default: Dummy } = require('src/components/UI/Styleguide/Dummy/');
const style = { backgroundColor: 'lightpink' };
<UILayout style={style} vertical>
  <Dummy>Block 1</Dummy>
  <Dummy>Block 2</Dummy>
</UILayout>;
```

Here is a more complexe exemple by nesting multiple `<UILayout>`:

```jsx
const { range, map } = require('ramda');
const styles = {
  layout: { backgroundColor: 'lightblue', padding: '10px' },
  avatar: { marginRight: '10px' },
  name: { fontSize: '14px' },
  title: { fontSize: '12px' },
};

<UILayout horizontal style={styles.layout} align>
  <div style={styles.avatar}>
    <UIAvatar firstName="Buzz" lastName="Aldrin" />
  </div>
  <UILayout vertical space="around">
    <div style={styles.name}>Buzz Aldrin</div>
    <div style={styles.title}>Astronaute</div>
  </UILayout>
</UILayout>;
```

Margin can also be added via the `gap` prop. The value is multiplied by the grid factor.

```jsx
const { default: Dummy } = require('src/components/UI/Styleguide/Dummy/');
const style = { backgroundColor: 'lightpink' };
<UILayout style={style} horizontal gap={1}>
  <Dummy>Block 1</Dummy>
  <Dummy>Block 2</Dummy>
</UILayout>;
```

The margin orientation depends on the layout direction.

```jsx
const { default: Dummy } = require('src/components/UI/Styleguide/Dummy/');
const style = { backgroundColor: 'lightpink' };
<UILayout style={style} vertical gap={1}>
  <Dummy>Block 1</Dummy>
  <Dummy>Block 2</Dummy>
  <Dummy>Block 3</Dummy>
  <Dummy>Block 4</Dummy>
</UILayout>;
```
