// @flow

import React from 'react';

import { cn } from '../shared/helpers';

import toDataAttributes from '../shared/object/toDataAttributes';
import styles from './UILayout.module.scss';

interface UILayoutProps {
  /**
   * Custom class name to define on UILayout root div
   */
  className?: string,

  /**
   * If set to true, direct children will be displayed horizontaly.
   * Equivalent of `flex-direction: row`
   */
  horizontal?: boolean,

  /**
   * If set to true, direct children will be displayed verticaly as a list.
   * Equivalent of `flex-direction: column`
   */
  vertical?: boolean,

  /**
   * Define spacing between elements.
   */
  space?: 'around' | 'between' | 'center' | 'start' | 'end',

  /**
   * Set to true to define children with width: 100%
   * meaning they will take as much width as of the parent
   */
  fill?: boolean,

  /**
   * Set to true to define children with flex: 1
   * meaning they will take as much space as they can
   */
  grow?: boolean,

  /**
   * Set to true to verticaly center children
   */
  align?: boolean,

  /**
   * Set the gap between each children.
   * This value is multiplied by the default grid factor.
   */
  gap?: number,

  children: React.ReactNode,

  viewRef?(element: HTMLDivElement): void,
};

/**
 * This component render a div with flexbox like styles applied to it.
 * This mostly allow to create complexe layout with nesting orientiation
 */
export const UILayout = ({
  className,
  horizontal,
  vertical,
  space,
  fill,
  grow,
  align,
  gap,
  children,
  viewRef,
  ...props
}: UILayoutProps) => {
  const dataAttributes = toDataAttributes({
    component: 'UILayout',
    horizontal,
    vertical,
    space,
    fill,
    grow,
    align,
    gap,
  });

  return (
    <div ref={viewRef} className={cn(styles.view, className)} {...dataAttributes} {...props}>
      {children}
    </div>
  );
};

export default UILayout;
